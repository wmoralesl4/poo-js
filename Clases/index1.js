

//Objetos literales

const nombre = "Wilson Morales"
const edad = 29

const usuario = {
    //**Atributos del Objeto */
    nombre: nombre,
    //Refactorizado, cuando es el mismo nombre, notacion EcmaScript6
    edad,
    apellido: "morales",
    hermanos: ["Karina", "Merlin"],
    padre: {
        nombre: "Alberto Morales",
        edad: 52
    },

    //Metodos del objeto, funciones = funcional
    presentarse(){
        //this es el alcance, scope
        console.log(`Hola mi nombre es ${this.nombre}`)
    },

    whoisyourfather(){
        console.log(`Mi padre es ${this.padre.nombre}`)
    }
    
}


// persona.nombre = "Wilson Alberto"
// persona.genero = "Masculino"
// console.log(persona)

usuario.whoisyourfather()


