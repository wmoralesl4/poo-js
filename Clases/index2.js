//******** Funcional
// function Usuario(nombres, apellidos, correo, activo){
//     this.nombres = nombres,
//     this.apellidos = apellidos,
//     this.correo = correo,
//     this.activo = activo
// }


//**********AHora Clases

class Usuario {
    constructor(nombres, apellidos, correo, activo){
        this.nombres = nombres,
        this.apellidos = apellidos,
        this.correo = correo,
        this.activo = activo    
    }

    presentarse(){
        return `Hola soy ${this.nombres} mi correo es ${this.correo}`
    }

    //Metodos getters y setters
    //Get --> obtener
    //Set --> dar

    //Como buena practica nunca acceder directamente a los atributos

    getNombre(){
        return this.nombres
    }

    getApellidos(){
        return this.apellidos
    }

    setNombre(newNombre){
        this.nombres = newNombre
    }
}

// ********* Herencia

class Profesor extends Usuario{
    constructor(nombres, apellidos, correo, activo, cursosDictados, calificacion){
        super(nombres, apellidos, correo, activo)
        this.cursosDictados = cursosDictados
        this.calificacion = calificacion
    }    
}

class Alumno extends Usuario{
    constructor(nombres, apellidos, correo, activo, cursosTomados){
        super(nombres, apellidos, correo, activo)
        this.cursosTomados = cursosTomados
    }    
}


/*
//Instancia de un objeto
const Wilson = new Usuario("Wilson", "Morales", "Wmoralesl4@umg", true)

// Wilson es una instancia de la clase usuario

Wilson.setNombre("Xadeon")
console.log(Wilson.getNombre())

*/

const wilson = new Alumno("Wilson", "Morales", "wmorales@umg", true, ["html", "css", "js"])
const arriaga = new Profesor("arriaga", "Morales", "wmorales@umg", true, ["html", "css", "js"], 10)

console.log(arriaga)

wilson.setNombre("Mario")

console.log(wilson)
