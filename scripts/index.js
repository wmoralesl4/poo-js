import Curso from "./clases/curso.js"
import Profesor from "./clases/profesor.js"
import Alumno from "./clases/alumno.js"

//Instancias Curso
/*const html = new Curso("HTML desde Cero", "https://edteam-media.s3.amazonaws.com/courses/medium/c32aad4d-6dd6-4ae5-aa86-8254069988df.png", 10)
const css = new Curso("Css desde Cero", "https://edteam-media.s3.amazonaws.com/courses/medium/82daef93-44b7-4a0a-9910-42aa609c6eee.png", 11)
const js = new Curso("JavaScript desde Cero", "https://edteam-media.s3.amazonaws.com/courses/medium/1fc49db5-2454-43e5-b94b-bed0d98719f2.png", 11)
*/

const elemCursos = document.getElementById("cursos")
const elemProfe = document.getElementById("profesores")
const selectCurso = document.getElementById("printCurso")

//Imprime los cursos
function mostrarCurso(curso){
    //Cada instancia se representa con un div
    const hijo = document.createElement("div")
    //Se agrego la clase card al div
    hijo.classList.add("card")
    hijo.innerHTML = `
    <div class="img-container s-ratio-16-9 s-radius-tr s-radius-tl">
        <img src="${curso.getPoster()}" alt="${curso.getNombre()}"/>
    </div>
    
    <div class="card__data s-border s-radius-br s-radius-bl s-pxy-2">
        <h3 class="t5 s-mb-2 s-center">${curso.getNombre()}</h3>
        
        <div class="s-center">
            <spam class="small">Cantidad de clases: ${curso.getClases()}</spam>    
        </div>
    </div>
    `
    elemCursos.appendChild(hijo) 
}

function mostrarProfesor(profesor){
    //Cada instancia se representa con un div
    const hijo = document.createElement("ul")
    //Se agrego la clase card al div
    hijo.classList.add("feature-list")
    hijo.innerHTML = `
    <h3>${profesor.getNombre()} ${profesor.getApellidos()}</h3>
    <li>Correo: ${profesor.getCorreo()}</li>
    <li>Cursos: ${profesor.getCursosDictados()}</li>
    <li>Calificacion: ${profesor.getCalificacion()}</li>
    `
    elemProfe.appendChild(hijo) 
}

function imprintCursos(curso){
    //Cada instancia se representa con un div
    const hijo = document.createElement("option")
    hijo.value = `${curso.getNombre()}`
    //Se agrego la clase card al div
    hijo.innerHTML = `${curso.getNombre()}`
    selectCurso.appendChild(hijo) 
}

function todosCursos(){
    curso.forEach(curso => imprintCursos(curso));
}

//Muestra en el doom los objectos
/*mostrarCurso(html)
mostrarCurso(css)
mostrarCurso(js)
*/

//Formulario para crear Cursos
const formulario = document.getElementById("formCursos")
formulario.addEventListener("submit", e => {
    e.preventDefault()
    const target = e.target
    console.log(target.nombreCurso.value)

    const curso = new Curso(target.nombreCurso.value, target.posterCurso.value, target.clasesCurso.value)
    mostrarCurso(curso)
})

//Formulario para crear Profesores
const formulario2 = document.getElementById("formProfesor")
formulario2.addEventListener("submit", e => {
    e.preventDefault()
    const target = e.target

    const profe = new Profesor(target.nombreProfe.value,target.apellidoProfe.value,target.correoProfe.value, true, null, target.calificacionProfe.value)
    mostrarProfesor(profe)
    console.log(profe)
})


//Creando profesores y alumnos
const profesor1 = new Profesor("Roberto", "Arriaga", "Arriga@UMG", true, ["CSS", "HTML"], 8)
const profesor2 = new Profesor("Charly", "Lima", "Charly@UMG", true, ["Compiladores", "Node.js"], 10)

const alm1 = new Alumno("Wilson", "Morales", "wmorales@umg", true, ["css", "html"])
const alm2 = new Alumno("Alberto", "Lazaro", "wmorales@umg", true, ["css", "html"])

// console.log(profesor)
// console.log(alm1)

const chtml = new Curso("html", "https://edteam-media.s3.amazonaws.com/courses/medium/c32aad4d-6dd6-4ae5-aa86-8254069988df.png", 7)

chtml.setInscritos([...chtml.getInscritos(), alm1])
console.log(chtml)

chtml.setInscritos([...chtml.getInscritos(), alm2])
console.log(chtml)

mostrarProfesor(profesor1)
mostrarProfesor(profesor2)

mostrarCurso(chtml)
imprintCursos(chtml)
todosCursos()