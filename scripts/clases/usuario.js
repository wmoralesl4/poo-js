export default class Usuario {
    constructor(nombres, apellidos, correo, activo){
        this.nombres = nombres,
        this.apellidos = apellidos,
        this.correo = correo,
        this.activo = activo    
    }
    presentarse(){
        return `Hola soy ${this.nombres} mi correo es ${this.correo}`
    }
    getNombre(){
        return this.nombres
    }
    getApellidos(){
        return this.apellidos
    }
    getCorreo(){
        return this.correo
    }

    setNombre(newNombre){
        this.nombres = newNombre
    }
}
